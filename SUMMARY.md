# Summary

## Overview

* [Overview](overview/overview.md)
* [Navigation](overview/navigation.md)
* [Dashboard](overview/dashboard.md)

## Attendance

* [Add  Attendance](attendance/add-attendance.md)
* [Attendance History](attendance/attendance-history.md)
* [Attendance Detail](attendance/attendance-detail.md)
* [View on map](attendance/view-on-map.md)

## Customer Support

* [Add Suport](support/add-support.md)
* [Support History](support/support-history.md)
* [Support Detail](support/support-detail.md)

## Salary

* [See Future Works](future-works/salary.md)


## Profile

* [View Profile](profile/view-profile.md)
* [Edit Profile](profile/edit-profile.md)
* [Reset Password](profile/reset-password.md)

## Employee

* [Add Employee](employee/add-employee.md)
* [Employee List](employee/employee-list.md)
* [Details of an Employee](employee/employee-detail.md)
* [Edit Employee information](employee/employee-edit.md)
* [Password Reset](employee/employee-reset-password.md)

## Notification

* [Add Notification](notification/add-notification.md)
* [Notification List](notification/notification-list.md)

## Messaging

* [See Future Works](future-works/messaging.md)

## Event
* [Add Event](event/add-event.md)
* [Event List](event/event-list.md)
* [Event Detail](event/event-detail.md)

## Technical Documentation
* [Concerns](technical-documentation/concern.md)
* [Attendance](technical-documentation/attendance.md)
* [Profile](technical-documentation/profile.md)
* [Employee](technical-documentation/employee.md)
* [Support](technical-documentation/support.md)
* [Notification](technical-documentation/notification.md)
* [Event](technical-documentation/event.md)
* [Auth](technical-documentation/auth.md)

## Future Works

* [Auth](future-works/auth.md)
* [Salary](future-works/salary.md)
* [Messaging](future-works/messaging.md)

