## Support Report

Show support history with limited information in recycle view.

![](../assets/support-history.png)![](../assets/support-history-marked.png)

<!-- Normal user can see only his information, moderator and admin can see everybody's information. -->

##### 1. sorting/search menu

##### 2. sort/searching

Search by ID: [select2](https://select2.org/dropdown) dropdown.

Search by date: a date range \(from-to\).

<!-- normal user can sort/search by only date. -->

##### 3. Date and month

##### 4. User/employee name

##### 5. Start time

##### 6. End time
##### 7. Organization

##### 9. Detail View

[shows detail of that record](support-detail.md)

[view technical documentation](../technical-documentation/support.md#support-history)

