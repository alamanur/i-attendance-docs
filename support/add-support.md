### Register a customer support info

Bottom-right \(add\) menu &gt; support

![](../assets/support-entry.png)![](../assets/support-entry-marked.png)

User can register a customer support from this page.

##### 1. Organization


##### 2. Start Time

A time picker display time in 12h format

##### 3. End Time

A time picker display time in 12h format

##### 4. Support Issue

##### 5. Person
A text area with hints 
```
eg:
Name:  Somebody
Designaton: Admin
Mobile: 01199 121 121
```

##### 6.Description
A text area
##### 7.Remarks

#### [Go to technical documentation](../technical-documentation/support.md#add-support) 



