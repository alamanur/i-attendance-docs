## Support Detail

Detail information of a single Customer Support Entry.

![](../assets/support-details.png)                 ![](../assets/support-detail-marked.png)

##### 1. Name of the Supporter

##### 2. Supporter Employee ID

##### 3. Support Date Date

##### 4. Organization/Customer

##### 5. Support Issue

##### 6. Person

##### 7. Description

##### 8. Remarks

[view technical documentation](../technical-documentation/support.md#support-detail)

