## Details of his Profile 


![](../assets/employee/employee-details.png)![](../assets/employee/employee-details-marked.png)

##### 1. Button for navigation menu

##### 2. Edit Profile

[nav to edit page](edit-profile.md)

##### 3. Reset Password

[nav to reset password page](reset-password.md)

##### 4. Avatar

Employee avatar. If null, than a default avatar.

##### 5. Avatar change

##### 6. Employee other information

[view technical documentation](../technical-documentation/profile.md#view-profile)

