## Edit Profile


![](../assets/employee/employee-edit.png)

Here informations are quite similar to [Add Employee](../employee/add-employee.md) page 
**Except** for the [Role, Password and confirm password](../employee/add-employee.md#add-employee-role) fields will not be here.

[view technical documentation](../technical-documentation/profile.md#edit-profile)