### Add Event


Bottom-right \(add\) menu &gt; Event

![](../assets/event/add-event.png)    ![](../assets/event/add-event-marked.png)


#### 1. Title
&nbsp;

#### 2. Time

A time picker

#### 3. Description

A textarea.

#### 4. Status

Public events can see anyone, where private events can see only that user.



#### [Go to technical documentation](../technical-documentation/event.md#event-list) 