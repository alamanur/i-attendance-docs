### Event Detail



![](../assets/event/event-detail.png)    ![](../assets/event/event-detail-marked.png)

#### 1. Event Title
First two or three words of title, if more than "..."

#### 2. Event Creator

&nbsp;

#### 3. Event Creator ID

&nbsp;

#### 4. Created At

&nbsp;

#### 5. Event Title

&nbsp;

#### 6. Event Description

&nbsp;

#### 7. Event Time

&nbsp;

#### 6. Event Date

&nbsp;


#### [Go to technical documentation](../technical-documentation/event.md#event-detail) 