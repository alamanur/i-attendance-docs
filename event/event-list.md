### Event List


![](../assets/event/events.png)    ![](../assets/event/events-marked.png)

#### 1. Search/sort
A date picker, searching must be by **"event time"**, not by created_at.

#### 2. User Avatar

&nbsp;

#### 3. Event Date

&nbsp;

#### 4. User Created the Event

&nbsp;

#### 5. Event Title

&nbsp;

#### 6. Event Detail

on click will navigate to [event detail page](event-detail.md).


#### [Go to technical documentation](../technical-documentation/event.md#event-list) 