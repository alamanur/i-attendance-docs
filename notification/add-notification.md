### Add Notification


Bottom-right \(add\) menu &gt; Notification

![](../assets/notification/add-notification.png)




#### [Go to technical documentation](../technical-documentation/notification.md#add-notification) 