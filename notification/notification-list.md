### Notification list



![](../assets/notification/notifications.png)     ![](../assets/notification/notifications-marked.png)

#### 1. Search/sort from a date range

If there is no notifications, there should be a message like "no notifications".


#### 2. Avatar of notification owner  
&nbsp;  

#### 3. Name of the notification owner   
&nbsp;  

#### 4. The Notification   
&nbsp;  

#### 5. Date of Notification   
&nbsp;  

#### [Go to technical documentation](../technical-documentation/notification.md#notification-list) 