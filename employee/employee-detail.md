## Details of Employee {#employee-detail}

Detail information of an Employee.
Normal users can't view this page.

![](../assets/employee/employee-details.png)                 ![](../assets/employee/employee-details-marked.png)

##### 1. Button for navigation menu

##### 2. Edit Profile

[nav to edit page](/employee-edit.md)

##### 3. Reset Password

[nav to reset password page](/employee-reset-password.md)

##### 4. Avatar

Employee avatar. If null, than a default avatar.

##### 5. Avatar change

##### 6. Employee other information

[view technical documentation](technical-documentatiojn/employee.md#employee-detail)

