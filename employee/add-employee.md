### Register a new employee/user 

Bottom-right \(add\) menu &gt; Employee

![](../assets/employee/new-employee.png?raw=true "Figure 1-1")   ![](../assets/employee/new-employee-marked.png)

Only admin can view and register/add a new employee/user from this page.
#### [Go to technical documentation for this page](../technical-documentation/employee.md#add-user) 

##### 1. User/Employee Avatar


##### 2. User/Employee ID

Must be Unique ( Default is last employee/user ID + 1 ).

##### 3. User/Employee Name

##### 4. User/Employee Email

Must be unique

##### 5. Phone No

##### 6. Designation

##### 7. Gender
A dropdown select
`Male, Female, Other`

##### 8. Address
A textarea

<a name="add-employee-role"></a>
##### <a name="add-employee-role"></a>8. Role

A dropdown select
[see technical doc for dropdown contents](/technical-documentatiojn/employee.md#add-user)

##### 9.Password

a password field

##### 10.Confirm Password

a password field


#### [Go to technical documentation for saving/storing User/Employee](/technical-documentatiojn/employee.md#save-user) 