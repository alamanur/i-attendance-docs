## Reset Password

Reset password of an employee
![](../assets/employee/employee-reset-password.png)
[view technical documentation](technical-documentatiojn/employee.md#employee-reset-password)