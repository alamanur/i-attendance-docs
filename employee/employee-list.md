## Employee List

Show All the employees with short information.
Normal users can't view this page and navigation.
![](../assets/employee/employee-list.png)![](../assets/employee/employee-list-marked.png)

##### 1. Employee Avatar

##### 2. Employee Avatar

If there is no avatar, than default avatar with employee name first alphabet

##### 3. Employee Name

##### 4. Employee Designation

##### 5. Employee Role

##### 6. View

[nav to detail information for that employee](/employee-detail.md)


[view technical documentation](technical-documentatiojn/employee.md#employee-list)

