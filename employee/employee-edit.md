## Edit Employee information

Edit information of an Employee.
Normal users can't view this page.

![](../assets/employee/employee-edit.png)

Here informations are quite similar to [Add Employee](/add-employee.md) page.

[view technical documentation](technical-documentatiojn/employee.md#employee-edit)