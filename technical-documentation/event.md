<a name="add-event"></a>
### Add Event

api url: `base-url/events/add`

method: `post`
params:

```
title: [input]
time: [input]
description: [input]
status: [public/private from radio]
```

response:  `[ id, title, time, description, status, created_at, user_name, user_avatar ]`


<a name="event-list"></a>
### Event List

api url: `base-url/events`

method: `get`
params:

```
from: [date from, optional]
to: [date to, optional]
```

response:  `[[ id, title, time, description, status, created_at, user_name, user_avatar ], [...], ... ]`



<a name="event-detail"></a>
### Event Detail

api call not required!

api url: `base-url/events/[id]`

method: `get`

response:  `[id, title, time, description, status, created_at, user_name, user_avatar ]`

