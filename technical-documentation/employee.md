<a name="add-user"></a>
###Add Employee
to view the page
api url: `base-url/users/add`

method: `get`
response:   
    Roles: `[ [role_id, display_name], [...], ...]`  
    Last employee id:  `[last_employee_id]`
Roles will be used in dropdown, `display name` will be shown `role_id` will be sent.
Last employee id will be shown as default in Employee ID input.

<a name="save-user"></a>
###Save Employee

api url: `base-url/users/add`

method: `post`
params:

```
id: [input]
name: [input]
email: [input]
phone: [input]
designation: [input]
gender: [male/female/other]
avatar: [file]
address: [input]
role_id: [select dropdown]
password: [input]
```

response:  `[ id, name, email, phone, designation,  gender, avatar, address, role ]`

<a name="employee-list"></a>
###Employee List

api url: `base-url/users`

method: `get`
<!-- params:

```
id: [input]
name: [input]
email: [input]
phone: [input]
designation: [input]
gender: [male/female/other]
avatar: [file]
address: [input]
role_id: [select dropdown]
password: [input]
```
 -->
response:  `[[ id, name, designation, role, avatar], [...],...]`


<a name="employee-detail"></a>
###Employee Detail

api url: `base-url/users/[id]`

method: `get`
params:

```
id: [id]
```

response:  `[ id, name, email, phone, designation,  gender, avatar, address, role ]`


<a name="employee-edit"></a>
###Edit Employee 

api url: `base-url/users/[id]/edit`

method: `get`

response:  `[ id, name, email, phone, designation,  gender, avatar, address, role ]` `[[role_id, display_name], [..], ...]`



<a name="employee-update"></a>
###Update Employee

api url: `base-url/users/[id]/edit`

method: `put`

param:  
```
id: [input]
name: [input]
email: [input]
phone: [input]
designation: [input]
gender: [male/female/other]
avatar: [file]
address: [input]
role_id: [select dropdown]
```

response:  `[ id, name, email, phone, designation,  gender, avatar, address, role ]`


<a name="employee-reset-password"></a>
### Reset Password

api url: `base-url/users/password-reset/[id]`

method: `put`

param:  
```
old_password: [input]
new_password: [input]
```

response:  `[ id, name, email, phone, designation,  gender, avatar, address, role ]`
