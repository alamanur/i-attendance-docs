### <a name="view-profile"></a> View Profile 

api url: `base-url/profile/view`

method: `get`
response:  `[ name, email, phone, designation,  gender, avatar, address, role ]`


### <a name="edit-profile"></a> Edit Profile

api url: `base-url/profile/edit`

method: `get`

response:  `[ name, email, phone, designation,  gender, avatar, address, role ]`


### <a name="update-profile"></a> Update Profile

api url: `base-url/profile/edit`

method: `put`

param:  
```
id: [input]
name: [input]
email: [input]
phone: [input]
designation: [input]
gender: [male/female/other]
avatar: [file]
address: [input]
```

response:  `[ id, name, email, phone, designation,  gender, avatar, address, role ]`

### <a name="reset-password"></a> Reset Password

api url: `base-url/profile/password-reset`

method: `put`

param:  
```
old_password: [input]
new_password: [input]
```

response:  `[ id, name, email, phone, designation,  gender, avatar, address, role ]`






