## 	Notifications

**In this version we will make notifications realtime using websocket. Currently we will be using [Pusher](https://pusher.com/) driver which will give 200k Messages / Day and 100 Max Connections in free account. In future we will try to go for node.js**


<a name="add-notification"></a>
### Add Notification

api url: `base-url/notifications/add`

method: `post`
params:

```
issue: [input]
```

response:  `[ id, issue, created_at ]`

<a name="notification-list"></a>
### Notification List 

api url: `base-url/notifications`

method: `get`
params:

```
from: [date from, optional]
to: [date to, optional]
```
response:  `[[ id, issue, created_at, user, user_avatar ] ]`
