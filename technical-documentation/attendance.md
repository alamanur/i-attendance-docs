<a name="add-attendence"></a>
### Add Attendence

##### to view the page:

Server must check if there is an entry for this

api url: `base-url/attendances/add`

method: `get`

response:  `[ id, in_time, in_location, in_latitude, in_longitude,  out_time, out_location, out_latitude, out_longitude, working_hour, over_time, remarks ]`

<a name="save-attendence"></a>
##### register today's attendance:

api url:`base-url/attendances/add`

method: `post`

params:

```
in_latitude: [gps]
in_longitude: [gps]
type: [check_in]
remarks: [input]
or
out_latitude: [gps]
out_longitude: [gps]
type: [check_out]
remarks: [input]
```

response:`id, in_time, in_location, out_time, out_location, working_hour, over_time, remarks`

<a name="attendences"></a>
### Attendance Report

api url:`base-url/attendances`

method: `get`

params:

```
user_id: [optional]
from: [date from, optional]
to: [date to, optional]
```

response:   `[ [id, in_time, in_location, out_time, out_location, working_hour, over_time, remarks], [...], ...]`

<a name="attendance-detail"></a>
### Attendance Detail

api url:`base-url/attendances/[id]`

method: `get`

response:

`[ id, user_id, user_name, in_time, in_location, in_latitude, in_longitude, out_time, out_location, out_latitude, out_longitude, working_hour, over_time, date, remarks ]`







