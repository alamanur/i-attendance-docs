### Authentication

##### Login:

api url: `base-url/login`

method: `post`

params:

```
email: [input]

password: [input]
```

response:

```
username, email, role, access_token,token_type, expires_in
```

[see future works](/future-works/auth.md)

##### Logout:

api url: `base-url/logout`

method: `post`

response: `message`

