### Add Support {#add-support}

api url: `base-url/supports/add`

method: `post`
params:

```
organizaton: [input]
start_time: [input, format(2017-09-09 06:05:24)]
end_time: [input, format(2017-09-09 06:05:24)]
support_issue: [input]
person: [input]
description: [input]
remarks: [input]
```

response:  `[ id, organizaton, start_time, end_time, support_issue,  person, description, remarks ]`

### Support History {#support-history}
api url:`base-url/supports`

method: `get`

params:

```
user_id: [optional]
from: [date from, optional]
to: [date to, optional]
```

response:   `[ [id, organization, support_issue, start_time, end_time, person, description, remarks, total_time], [...], ...]`


### Support History {#support-detail}
api url:`base-url/supports/[id]`

method: `get`

response:   
`[ id, organization, support_issue, person, description, start_time, end_time, remarks, total_time, created_at]`


