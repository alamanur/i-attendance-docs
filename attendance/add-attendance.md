### Register today's attendance

Bottom-right \(add\) menu &gt; Attendance

![](../assets/todays-attendence.png)![](../assets/add-attendence-marked.png)

User can register today's attendance from this page.

##### 1. Check In time

Initially the text is "Check in" and the text in no.6 is also "Check In". User registers his check in time by clicking this button \( no.6 \). After check in the text \(no 1\) will be the check in time of the user and background color will be according to this time.

\(eg. for before 10:00 AM -&gt; Green, after 10:00 AM -&gt; Yellow, after 10:30 AM -&gt;  Red etc....\)

##### 2. Check Out Time

Same as check in. After user checks in, no. 6 button text will be "check out". and after checkout this button will be invisible.

##### 3. Check in location in short

after user checks in, his check in information is saved in database. this portion will be shown if the user checks in.   same will be for checkout.

##### 4. View Location

on click of this button a new page will open showing his check in location in google map.

##### 5. Remarks

any remarks regarding users today's attendance.

##### 6.Submit

will be invisible after check in and check out.


**Concerns:** Attendence can not be registered more than once a day
 
#### [Go to technical documentation](../technical-documentation/attendance.md#add-attendence) 



