## Attendance History

Show attendance history with limited information in recycle view.

![](../assets/attendance-history.png)![](../assets/attendence-history-marked.png)

Normal user can see only his information, moderator and admin can see everybody's information.

##### 1. sorting/search menu

##### 2. sort/searching

Search by ID: [select2](https://select2.org/dropdown) dropdown.

Search by date: a date range \(from-to\).

normal user can sort/search by only date.

##### 3. Date and month

##### 4. User/employee name

##### 5. Position accuracy

if with gps -&gt; red,

if without gps -&gt; grey

##### 6. In time

##### 7. Out time

##### 9. Detail View



**Concerns:**   
- api request should be paginated
- user privilege should be in concern
- gps access is not mandatory, but preferred. 

[shows detail of that record](attendance-detail.md)

[view technical documentation](../technical-documentation/attendance.md#attendances)

