## Attendance Detail

Detail information of a single attendance entry.

![](../assets/attendance-details.png)                 ![](../assets/attendance-detail-marked.png)

##### 1. Name of the employee

##### 2. Employee ID

##### 3. Entry Date

##### 4. Check In Time \( with color marking according to entry time \)

##### 5. Location Detail

##### 6. [View on google map](/view-on-map.md)

##### 7. Working Hour

##### 8. Over Time

##### 9. Remarks

Any Remarks for that entry.

[view technical documentation](/technical-documentation/attendance.md#attendance-detail)

