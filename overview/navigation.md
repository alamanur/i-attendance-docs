### Navigation

![](../assets/navigation.png)

Main Menu:

1. Attendence \(give today's attendence\)
2. Attendence History/Records \(View the attendence history with user privilege \)
3. Support History/Records \( View the attendence history with user privilege \)
4. Salary statement \(View the salary overview with user privilege\)
5. Profile \( view personal profile and edit/update information\)
6. Employees \( view other users profile and edit/update information with user privilege\). normal user can't see it.
7. Setting
8. Logout

Top-right menu:

1. Notifications \(View latest notifications with highlighted unread notifications \)
2. Messages \( personal messaging between users \)
3. Event \( view latest events\)
4. Logout

Bottom-right \(add\) menu

1. Attendence \( Today's attendence\)
2. Event \(add an event\)
3. Notification \(Add a custom notification\)
4. Employee \(add a new user\)



