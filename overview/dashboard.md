### Dashboard

![](../assets/dashboard.png)

Dashboard contains 6 option/menus/navs,

1. Notifications \( nav to notification with number of unread notification \(here:  5\) \)
2. Events \( nav to events with number of upcoming events from now \(here:  5\) \)
3. Supports \( nav to support history\)
4. Attendances \( nav to attendance history/report\)
5. Employees \( nav to employees with number of registered employees \). For normal user it will be nav to Profile.
6. Help \( user guide \)



