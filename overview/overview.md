## Overview

![](../assets/dashboard.png)  


i-attendance is a simple attendance management system with some related other features. The app consists of following features/ modules...

- [Attendance](../attendance/add-attendance.md)  
Keep records of attendance of an employee with time and location.
- [Customer Support](../support/add-support.md)
Customer support information with Duration.
- Salary  
Salary statement of an employee with working hour and over time reports.
- [Employee](../employee/employee-list.md)
Employee management
- [Notifications](../notification/notification-list.md)
Notifications to inform/notify other members.
- Messaging  
Communication between employees
- [Event](event/event-list.md)
Raise an event and notify others.



